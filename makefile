#------------------------------------------------------------------------------
# Makefile for CSE 130 Programming Assignment 1
# CruzID: adion Term: 2022 Fall CSE130
#
# make split             makes split
# make clean             removes all binaries
# make valgrind	         checks split for memory errors (NOTE: no outputs)
#------------------------------------------------------------------------------

#===COMP LANG===#
CC = clang

#===FLAGS===#
CFLAGS = -g -Wall

#===TARGETS===#
TARGET = split
FILE = "input.txt"

split: split.c
	$(CC)  $(CFLAG) -o $(TARGET) $(TARGET).c

valgrind:
	valgrind ./split $(FILE) a >/dev/null

clean:
	rm -f $(TARGET) *.o
