//===LIBRARY===//
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <err.h>

int main(int argc, char* argv[]) {

    //===INITALIZATIONS===//            
    const int MAX = 1000;           //Makes changing buffer sizes easier.
    char buf[MAX];                  //Buffer for file reading.
    char printBuf[MAX];             //Copy buffer for printing
    int readRet = 0;                //Return variable for read(). 
    int filePointer = 0;            //Flag variable for open()
    char Delim = *argv[argc - 1];   //Delimiter storage. 

    //===PREFLIGHT CHECK===//
    if(argc < 3){
        //check if correct number of arguments.
        warnx("not enough arguments\nusage: ./split <files> <delimiter>");
        return 1;
    }

    if(strlen(argv[argc-1])>1){
        //check if single character delim. 
        warnx("only single-character delimiters allowed\nusage: ./split <files> <delimiter>");
        return 1;
    }

    //===MAIN LOOP===//
    for (int i = 1; i < argc - 1; i++) {
        //NOTE: Last argc is reserved for delimiter. 

        //===FILE INPUT TYPE===//
        if(*argv[i] != '-'){
            filePointer = open(argv[i], O_RDONLY);

            //===CHECK FILE VALID===//
            if (filePointer < 0) {
                warnx("%s: No such file or directory", argv[i]);
                continue;   //Skip file and move onto to next file. 
            }
        }
        else{
            filePointer = 0;    //Sets to stdin
        }

        readRet = 1; //Force Loop to start

        //===PRINT LOOP===//
        while(readRet > 0){

            readRet = read(filePointer, buf, MAX); //Read file and overide force loop. 

            //===COPY LOOP===//
            for (int j = 0; j < readRet; j++) {
                if (buf[j] != Delim) {
                    printBuf[j] = buf[j];
                }
                else {
                    printBuf[j] = '\n';
                }
            }
            write(1, printBuf, readRet); //Prints Statement. 
        }
        close(filePointer);
    }
    return 0;
}
