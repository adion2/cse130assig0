   CruzID: Adion
   2022 Fall CSE130 Assignment0
=================================================================================

Packet Description
=================================================================================
Split.c: the uncompiled C code for the project. 
Split: Precompiled C code for the project (Fallback incase the makefile somehow gets corrupted).
MAKEFILE: compiles the C code and allows for valgrind test to be preformed. (NOTE: valgrind will not show split output.) Target directories in the makefile can be modified easily. 
README.md: This file. 

Compiling
=================================================================================
make split -> compiles split
make valgrind -> runs valgrind test
make clean -> removes the compiled split. 

Design Documentation
=================================================================================
The program is designed to loop through multiple files and user inputs and split input lines based on a delimiter character. The program is designed to make a distinction between the user input and the file input. The program is built with 2 loops. 
Below is a rough layout of the program:

Basic Checks
File Loop
| User/File Check
| Copy Loop
| | Check for Delimiter and split
| Print Copy
| Close File

The program uses 2 character arrays, a buffer array and a print array. The print array is designed to copy the values from the comparison (delimiter or not) and hold onto them until printing. After it is printed, it is considered to store only garbage values and is overwritten. The buffer only stores the read and thus once it is done with the copy loop, it too is considered garbage. In addition, the program will skip files it can not read. It also is built in with checks to ensure a bad input is not accepted, such as multi character delimiter or not enough arguments. 

Design Choices
=================================================================================
I tried to make the program as compact and light as possible. The program had undergone several itterations before I evently narrowed it down to as simple as a loop as possible. My goal was to avoid as much complexity as possible. Because of this more focus on simplicity, one thing I made sure was that the program had to work with various buffer sizes. Thus, I designed the code to be able to quickly swap out buffer sizes with a single variable. The current buffer size of 1000 was a abritary choice. It seemed like the easiest number for people to read. It was previously at a much larger value, 4096 (as it is the average size of a webpage), and I also tested in 512 (simply because it was a nice number to test). 
