# Tests for valgrind on a input file
text="test_files/input.txt"

diff <( valgrind ./split $text a) <(valgrind ./rsplit $text a) > /dev/null
